module.exports = {
  root: true,
  extends: [
    'airbnb-base',
    'eslint:recommended',
    'plugin:jest/recommended',
    'plugin:jest/style',
    'plugin:prettier/recommended',
  ],
  env: { es6: true, node: true },
  parserOptions: { ecmaVersion: 9 },
  rules: {
    'jest/consistent-test-it': ['error', { fn: 'it' }],
    'jest/prefer-strict-equal': 'error',
  },
};
