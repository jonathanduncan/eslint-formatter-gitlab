const path = require('path');

const eslint = require('eslint');
const yaml = require('js-yaml');
const { fs, vol } = require('memfs');

function setup(testEnv, gitlabCI, gitlabCIFile = '/build/.gitlab-ci.yml') {
  process.env = testEnv;
  jest.restoreAllMocks();
  jest.resetModules();
  jest.spyOn(process, 'cwd').mockReturnValue('/build');
  jest.setMock('eslint', eslint);
  jest.setMock('fs', fs);
  vol.reset();
  vol.fromJSON({
    [gitlabCIFile]: yaml.safeDump(gitlabCI),
  });
  // eslint-disable-next-line global-require
  return require('.');
}

it('should write a code quality report', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([
    {
      filePath: path.join(process.cwd(), 'filename.js'),
      messages: [{ line: 42, message: 'This is a linting error', ruleId: 'linting-error' }],
    },
  ]);
  expect(JSON.parse(vol.readFileSync('/build/output.json'))).toStrictEqual([
    {
      description: 'This is a linting error',
      fingerprint: '2235367ffc3cbeb18474a7285f9d5803',
      location: { lines: { begin: 42 }, path: 'filename.js' },
    },
  ]);
});

it('should throw if the output location is empty', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: '' } } },
    },
  );
  expect(() => formatter([])).toThrow(
    new Error(
      'Expected test-lint-job.artifacts.reports.codequality to be one exact path, but no value was found.',
    ),
  );
});

it('should throw if the output location is an array', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: [] } } },
    },
  );
  expect(() => formatter([])).toThrow(
    new Error(
      'Expected test-lint-job.artifacts.reports.codequality to be one exact path, but found an array instead.',
    ),
  );
});

it('should not fail if a rule id is null', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([
    {
      filePath: path.join(process.cwd(), 'filename.js'),
      messages: [{ line: 42, message: 'This is a linting error', ruleId: null }],
    },
  ]);
  expect(JSON.parse(vol.readFileSync('/build/output.json'))).toStrictEqual([
    {
      description: 'This is a linting error',
      fingerprint: '8e905c6c557d0066e5a01827b6216be6',
      location: { lines: { begin: 42 }, path: 'filename.js' },
    },
  ]);
});

it('should skip the output if CI_JOB_NAME is not defined', () => {
  const formatter = setup(
    {},
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([]);
  expect(vol.existsSync('/build/output.json')).toBe(false);
});

it('should respect the ESLINT_CODE_QUALITY_REPORT environment variable', () => {
  const formatter = setup(
    {
      ESLINT_CODE_QUALITY_REPORT: 'elsewhere.json',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  formatter([]);
  expect(JSON.parse(vol.readFileSync('/build/elsewhere.json'))).toStrictEqual([]);
});

it('should return the value of the default reporter', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  const result = formatter([
    {
      filePath: path.join(process.cwd(), 'filename.js'),
      errorCount: 1,
      warningCount: 0,
      messages: [
        { line: 42, message: 'This is a linting error', ruleId: 'linting-error', severity: 2 },
      ],
    },
  ]);
  expect(result).toBe(`
/build/filename.js
  42:0  error  This is a linting error  linting-error

✖ 1 problem (1 error, 0 warnings)
`);
});

it('should delegate the output using the ESLINT_FORMATTER environment variable', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      ESLINT_FORMATTER: 'custom-formatter',
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  const customFormatter = jest.fn().mockReturnValue('ESLint output');
  jest.spyOn(eslint.CLIEngine, 'getFormatter').mockReturnValue(customFormatter);
  const result = formatter([
    {
      filePath: path.join(process.cwd(), 'filename.js'),
      messages: [{ line: 42, message: 'This is a linting error', ruleId: 'linting-error' }],
    },
  ]);
  expect(eslint.CLIEngine.getFormatter).toHaveBeenCalledWith('custom-formatter');
  expect(result).toBe('ESLint output');
});

it('should fallback to stylish if the formatter itself is specified as output formatter', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      ESLINT_FORMATTER: require.resolve('.'),
    },
    {
      'test-lint-job': { artifacts: { reports: { codequality: 'output.json' } } },
    },
  );
  const result = formatter([
    {
      filePath: path.join(process.cwd(), 'filename.js'),
      errorCount: 1,
      warningCount: 0,
      messages: [
        { line: 42, message: 'This is a linting error', ruleId: 'linting-error', severity: 2 },
      ],
    },
  ]);
  expect(result).toBe(`
/build/filename.js
  42:0  error  This is a linting error  linting-error

✖ 1 problem (1 error, 0 warnings)
`);
});
