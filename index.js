const crypto = require('crypto');
const fs = require('fs');
const path = require('path');

const { CLIEngine } = require('eslint');
const yaml = require('js-yaml');

const {
  // Used as a fallback for local testing.
  CI_CONFIG_PATH = '.gitlab-ci.yml',
  CI_JOB_NAME,
  CI_PROJECT_DIR = process.cwd(),
  ESLINT_CODE_QUALITY_REPORT,
  ESLINT_FORMATTER,
} = process.env;

function getOutputPath() {
  const jobs = yaml.load(fs.readFileSync(path.join(CI_PROJECT_DIR, CI_CONFIG_PATH), 'utf-8'));
  const { artifacts } = jobs[CI_JOB_NAME];
  const location = artifacts && artifacts.reports && artifacts.reports.codequality;
  const msg = `Expected ${CI_JOB_NAME}.artifacts.reports.codequality to be one exact path`;
  if (!location) {
    throw new Error(`${msg}, but no value was found.`);
  }
  if (Array.isArray(location)) {
    throw new Error(`${msg}, but found an array instead.`);
  }
  return path.resolve(CI_PROJECT_DIR, location);
}

function createFingerprint(filePath, message) {
  const md5 = crypto.createHash('md5');
  md5.update(filePath);
  if (message.ruleId) {
    md5.update(message.ruleId);
  }
  md5.update(message.message);
  return md5.digest('hex');
}

function convert(results) {
  return results.reduce(
    (acc, result) => [
      ...acc,
      ...result.messages.map((message) => {
        const relativePath = path.relative(CI_PROJECT_DIR, result.filePath);
        // https://github.com/codeclimate/spec/blob/master/SPEC.md#data-types
        return {
          description: message.message,
          fingerprint: createFingerprint(relativePath, message),
          location: {
            path: relativePath,
            lines: {
              begin: message.line,
            },
          },
        };
      }),
    ],
    [],
  );
}

module.exports = (results) => {
  if (CI_JOB_NAME || ESLINT_CODE_QUALITY_REPORT) {
    const data = convert(results);
    const outputPath = ESLINT_CODE_QUALITY_REPORT || getOutputPath();
    const dir = path.dirname(outputPath);
    fs.mkdirSync(dir, { recursive: true });
    fs.writeFileSync(outputPath, JSON.stringify(data, null, 2));
  }
  let formatter = CLIEngine.getFormatter(ESLINT_FORMATTER);
  if (formatter === module.exports) {
    formatter = CLIEngine.getFormatter();
  }
  return formatter(results);
};
